# If tty (without X)
if [[ "$TERM" = "linux" ]]; then
	setfont cyr-sun16
	# Without this - I see tofu (squares) on Cyrillic output from translate-shell
	# See available fonts at /usr/share/consolefonts/
	# https://wiki.gentoo.org/wiki/Fonts#Non-latin_font_in_TTY_.28in_for_example_Ctrl-Alt-F2.29
fi
