# Default is 500, see
# https://unix.stackexchange.com/questions/163371
HISTFILESIZE=20000
HISTSIZE=20000

export NPM_CONFIG_PREFIX=$HOME/.local/
export PATH="/home/$USER/go/bin:~/.cargo/bin:/home/$USER/.local/bin:$NPM_CONFIG_PREFIX/bin:~/.local/share/junest/bin:/home/$USER/Applications:$PATH"
# We have this against messing with Portage files.
# Bonus: now you can `npm install -g` without root.
# According to
# https://wiki.gentoo.org/wiki/Node.js#npm
# https://stackoverflow.com/a/63026107/1879101
# https://www.reddit.com/r/Gentoo/comments/ydzkml/nodejs_is_it_ok_to_install_global_packages/

export XDG_DOWNLOAD_DIR=~/Downloads/
# Without this 1password cannot download attachments

export GOFLAGS=-tags=unit_tests,integ_tests
# Without this: {Neo}Vim (through gopls) do not lint test files

# I need this for GPG work, for Gentoo Guru commits
export GPG_TTY=$(tty)

export GTK_THEME=Adwaita:dark
# To have Gvim dark, also affect Transmission-GTK
# https://unix.stackexchange.com/a/709223/34318

# To have QT apps with dark theme, for example Wireshark, NixNote (when not inside AppImage)
export QT_QPA_PLATFORMTHEME=qt5ct

export AWS_SDK_LOAD_CONFIG=1

export AWS_REGION=eu-west-1

export AWS_VAULT_BACKEND=file

export LOCAL_DDB_ENDPOINT="http://localhost:8000"

GIT_PROMPT_ONLY_IN_REPO=1
GIT_PROMPT_START_ROOT="_LAST_COMMAND_INDICATOR_ \033[01;31m\]\w\[\033[00m\]"
source /home/vitaly/.config/bash/bash-git-prompt/gitprompt.sh

source /usr/share/bash-completion/completions/git

source /usr/share/bash-completion/completions/fzf
source /usr/share/fzf/key-bindings.bash
