# TODO add this to package.json of every service
alias yarn-linked="find . -type l | grep -v .bin | sed 's/^\.\/node_modules\///'"
alias yarn-unlink-all="yarn-linked | xargs yarn unlink && yarn install --check-files"
# From https://stackoverflow.com/a/60489061/1879101

alias icat="kitty +kitten icat"

alias ge="/usr/bin/geeknote"

alias g=git
alias s='git status'
alias d='git diff'
alias ga="git add --patch"
alias m='git commit --message'
alias am='git commit --all --message' # `git commit -am` or `g c -am`
alias push='git push'
alias pull='git pull'
alias log='git log'
alias lo='git log --pretty="%C(Yellow)%h  %C(reset)%ad (%C(Green)%cr%C(reset))%x09 %C(Cyan)%an: %C(reset)%s" --date=short prod..master'

# Weather from https://github.com/chubin/wttr.in
alias w='curl wttr.in/batumi'
alias w2='curl v2d.wttr.in/batumi'

# Print only ipv4 address
# From https://github.com/trailofbits/algo/blob/master/docs/client-linux-wireguard.md#configure-wireguard
alias ip4='curl ipv4.icanhazip.com'

function amp() {
	git commit --all --message "$1"
	git push && : # Without && : `git push` will accept commit message text
}

# See docs at https://pkgcore.github.io/pkgdev/man/pkgdev/commit.html
function pkg() {
	git add .

	# Multiple message - for "Closes: ..."
	pkgdev commit --message "$1"

	git push && : # Without && : `git push` will accept commit message text
}

# From https://linuxize.com/post/how-to-create-bash-aliases
function mkcd()
{
  mkdir -p -- "$1" && cd -P -- "$1"
}

function all() {
	git switch -c $1
	git add .
	git commit -m $1
	url=$(git push 2>&1 | grep --max-count=1 --only-matching https://github.com/River-Island/\\S*)
	xdg-open $url

	# Lines below - to set local branch with remote.
	# Without it - on `pull` after `all` you will get:
	# There is no tracking information for the current branch.
	# Please specify which branch you want to merge with.
	# See git-pull(1) for details.

		# git pull <remote> <branch>

	# If you wish to set tracking information for this branch you can do so with:

		# git branch --set-upstream-to=origin/<branch> mybranch
	sleep 5 # Wait when branch will be created on origin
	git fetch
	git branch --set-upstream-to=origin/$1 $1
}
# url parsing according to https://stackoverflow.com/a/53252012/1879101

# Run e2e with msg highlight
function e2e() {
	cd ./e2e
	ginkgo -r -v --trace --flake-attempts=1 | egrep --color '"msg":"([^"\\]|\\.)*"|$'
	cd -
}

# Run e2e with msg highlight
function e2e-local() {
	cd ./e2e
	ginkgo -r -v --trace --flake-attempts=2 --nodes=10 --keep-separate-reports | egrep --color '"msg":"([^"\\]|\\.)*"|$'
	cd -;
}

# Play latest record
function p() {
	P=~/record/out/
	mpv "$P/$(ls -t ~/record/out/ | head -n1)"
}

alias dev='aws-vault exec inventory_dev'
alias staging='aws-vault exec inventory_staging'
alias sf='aws-vault exec dev_sf'

alias e=vim
alias n=nvim

alias f='name=$(fzf --preview "cat {}") && vim "$name" && echo "$name"'
# I tried simpler `vim $(fzf)', but the problem is that after Vim exit -
# I want to see in bash history - what file was opened.

alias t=trans

# Against of typing ' ' around my input
alias tp="t :ru -p -join-sentence $1"

alias be='trans :be -I'

alias tre='tree -I node_modules -I dist'

alias l='ls -lhS --hyperlink=auto'
alias д='ls -lhS --hyperlink=auto'

alias св=cd

alias вфеу=date

# Adapted from https://stackoverflow.com/a/2622350/1879101
alias gr='grep --exclude-dir=node_modules --exclude-dir=dist --color=always -C9 -Rf /dev/stdin <<<'
# --color=always to remain color after piping, for example gr create_prefix --color=always | sed 's/\\n/\'$'\n''/g'

alias mg='kitty +kitten hyperlinked_grep --smart-case -C 9 "$@"'
# Grep, click to link - open in Vim, exact line

# With line numbers (git config grep.lineNumber), with .gitignore, with pathes only once (--heading)
alias ggr='git grep --heading -f /dev/stdin <<<'
# --no-index for outside of the repo, but in such case .gitignore does not count :(

alias sd='sdcv --color'
